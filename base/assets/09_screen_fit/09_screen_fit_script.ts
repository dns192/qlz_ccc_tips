// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import BGScaler from './bg_scaler';

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(BGScaler)
    bg: BGScaler = null;

    @property(cc.Node)
    pages: Array<cc.Node> = [];

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.onPageChanged(null,0);
    }

    onPageChanged(event,arg){
        for(var i = 0; i < this.pages.length; ++i){
            this.pages[i].active = false;
        }
        var index = Number(arg);
        this.pages[index].active = true;
    }

    onBGScalerChanged(event,arg){
        var mode = Number(arg);
        this.bg.setMode(mode);
    }

    // update (dt) {}
}
